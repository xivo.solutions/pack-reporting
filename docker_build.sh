#!/usr/bin/env bash
set -e

GIT_FULLSTATS=https://gitlab.com/xivo.solutions/xivo-full-stats.git
GIT_DBREPLIC=https://gitlab.com/xivo.solutions/xivo-db-replication.git

cleanup_build() {
	docker stop pgxivocc_test
	docker rm -f pgxivocc_test pack-reporting-test-fake-dbreplic pack-reporting-test-fake-fullstats
    docker rmi xivoxc/pack-reporting-test:"$TARGET_VERSION"
	rm -rf db-replic
	rm -rf full-stats
	#git reset --hard
}

trap cleanup_build EXIT

clone_project() {
	repo=$1
	shift
	path=$1
	shift
	branch=$1

	rm -rf $path
	git clone -b $branch --depth 1 $repo $path || git clone -b master --depth 1 $repo $path 
	
}

if [ -z $TARGET_VERSION ]; then
    echo "TARGET_VERSION is not available"
    exit -1
fi

print_important_step() {
    echo "##########"
    echo "## $1"
    echo "#######################"
}


print_important_step "Building Main Image"
echo "VERSION='$TARGET_VERSION'" > pack_reporting/version.py
docker build --build-arg TARGET_VERSION=$TARGET_VERSION -t xivoxc/pack-reporting:$TARGET_VERSION .

print_important_step "Building Test Image"
docker build --no-cache --build-arg TARGET_VERSION="$TARGET_VERSION" -t xivoxc/pack-reporting-test:"$TARGET_VERSION" -f tests/Dockerfile.test .

print_important_step "Preparing test environment..."

# Clone side projects
CLONE_BRANCH=${TARGET_VERSION:0:7}
clone_project $GIT_FULLSTATS full-stats $CLONE_BRANCH
clone_project $GIT_DBREPLIC db-replic $CLONE_BRANCH

XIVO_DB_REPLICATION=$(pwd)/db-replic
XIVO_FULL_STATS=$(pwd)/full-stats

LIQUIBASE_IMG="xivoxc/javaliquibase:1.5.0"

# Create test DB
docker run \
	--rm \
	--name pgxivocc_test \
	-e POSTGRES_PASSWORD=test \
	-d \
	xivoxc/pgxivocc
sleep 5
docker run \
	--rm \
	--name pack-reporting-test-fake-dbreplic \
	--env DB_PORT=5432 \
	--env DB_NAME=xivo_stats \
	--env DB_USERNAME=asterisk \
	--env DB_PASSWORD=proformatique \
	--link pgxivocc_test:db \
	-v $XIVO_DB_REPLICATION/src/universal/sqlscripts:/opt/liquibase/sqlscripts \
	"${LIQUIBASE_IMG}" /usr/local/bin/start.sh
docker run \
	--rm \
	--name pack-reporting-test-fake-fullstats \
	--env DB_PORT=5432 \
	--env DB_NAME=xivo_stats \
	--env DB_USERNAME=asterisk \
	--env DB_PASSWORD=proformatique \
	--link pgxivocc_test:db \
	-v $XIVO_FULL_STATS/src/universal/sqlscripts:/opt/liquibase/sqlscripts \
	"${LIQUIBASE_IMG}" /usr/local/bin/start.sh

print_important_step "Launching Unit Test"
docker run \
	--rm \
	--link pgxivocc_test:db \
	--entrypoint nosetests \
	xivoxc/pack-reporting-test:"$TARGET_VERSION" -v --with-xunit pack_reporting