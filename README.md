# Pack reporting

Statistic-related cron tasks for XiVOcc.


## Documentation

    http://xivocc.readthedocs.org

## How to develop :

The code is written in Python.

**Prerequistes:**
- postgresql-server-dev
- python-psycopg2
- sqlalchemy

<pre>
    $ export PYTHONPATH=$PYTHONPATH:$(pwd)
    $ export LOG_DIRECTORY=$(pwd)/logs
    $ export REPORTING_DATABASE_NAME=xivo_stats
    $ export REPORTING_SERVER=localhost
</pre>    


### Releasing ###

Before releasing a new version, please update the pack_reporting/version.py file.

## Testing

**Prerequistes:**
- docker installed
- source code of xivo-db-replication and xivo-full-stats
- you may define LOG_DIRECTORY, REPORTING_SERVER and REPORTING_DATABASE_NAME

<pre>
    $ export PYTHONPATH=$PYTHONPATH:$(pwd)
    $ export XIVO_DB_REPLICATION=path/to/xivo-db-replication
    $ export XIVO_FULL_STATS=path/to/xivo-full-stats
    $ ./launch-tests.sh
</pre>

## Build Docker image

    $ docker build -t xivoxc/pack-reporting:${TAG} .

## Recalculate statistics

Connect to the docker container and

    export PYTHONPATH=/usr/lib/python2.7/dist-packages/
    /usr/local/bin/specific-stats fill_db --start='2016-04-01 00:00:00' --end='2016-05-01 00:00:00'

## Closing calls

### Updating call_on_queue (old model) with xc_queue_call end_time

:warn: **Works from Deneb only**

Currently full stat can miss some hangup time for (as far as we know) queue calls.
See [#3634](https://projects.xivo.solutions/issues/3634).

Most of these hangup time are not missed by the new model and `xc_queue_call` table.

You'll find [a script to close call_on_queue based on xc_queue_call end_time](utils/fix-cq-hangup-time.sh) in the utils dir.

It can be used in a crontab on reporting host to be launched every 15min.

### Closing missed calls

Currently full stat can miss some hangup time for (as far as we know) queue calls.
See [#3634](https://projects.xivo.solutions/issues/3634).

The [utils/fix-cq-hangup-time.sh](utils/fix-cq-hangup-time.sh) can be used to close them 
But this script can still miss some:
- if new model miss the hangup time too (no bug known as of writing this, but ...)
- or uniqueid mis match (the above script joins call_on_queue and xc_queue_calls with uniqueid but they may not match)

Therefore there is still the possibility to also use this [script to close missed long calls](utils/close-missed-long-calls.sh) that can help to mitigate the issue.


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.