FROM python:2.7.16-slim-buster

ARG TARGET_VERSION
RUN apt-get update  --fix-missing \
    && mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7 \
    && apt-get install -y cron postgresql-client gawk

ADD requirements.txt /tmp/
ADD setup.py /tmp/
ADD pack_reporting /tmp/pack_reporting
ADD bin /tmp/bin
WORKDIR /tmp/
RUN ["pip", "install", "-r", "requirements.txt"]
RUN ["python", "setup.py", "install"]
ADD get_version.sh /tmp/
WORKDIR /opt/docker/conf/
WORKDIR /tmp/
RUN /tmp/get_version.sh > /opt/docker/conf/appli.version

LABEL version=${TARGET_VERSION}

ADD cron /cron
RUN crontab /cron/pack-reporting

ENTRYPOINT echo "WEEKS_TO_KEEP=$WEEKS_TO_KEEP\nREPORTING_DATABASE_NAME=$REPORTING_DATABASE_NAME" >> /etc/environment && cron -f
