DROP TABLE IF EXISTS "queue_log" ;
CREATE TABLE "queue_log" (
 "time" VARCHAR(26) DEFAULT ''::VARCHAR NOT NULL,
 "callid" VARCHAR(32) DEFAULT ''::VARCHAR NOT NULL,
 "queuename" VARCHAR(50) DEFAULT ''::VARCHAR NOT NULL,
 "agent" VARCHAR(50) DEFAULT ''::VARCHAR NOT NULL,
 "event" VARCHAR(20) DEFAULT ''::VARCHAR NOT NULL,
 "data1" VARCHAR(30) DEFAULT ''::VARCHAR,
 "data2" VARCHAR(30) DEFAULT ''::VARCHAR,
 "data3" VARCHAR(30) DEFAULT ''::VARCHAR,
 "data4" VARCHAR(30) DEFAULT ''::VARCHAR,
 "data5" VARCHAR(30) DEFAULT ''::VARCHAR,
 "id" SERIAL PRIMARY KEY
 );

CREATE INDEX queue_log__idx_time ON queue_log USING btree ("time");
CREATE INDEX queue_log__idx_callid ON queue_log USING btree ("callid");
CREATE INDEX queue_log__idx_queuename ON queue_log USING btree ("queuename");
CREATE INDEX queue_log__idx_event ON queue_log USING btree ("event");
CREATE INDEX queue_log__idx_agent ON queue_log USING btree ("agent");
