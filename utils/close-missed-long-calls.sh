#!/bin/bash

# This program is to be laucnhed in a crontab on host of the reporting serve
# Basically it fixes old model which closes call only when a new queue_log/cel arrives
# therefore some of the last calls of the day may stay "opened" and are not catched by the force close mechanism

# This program can be put in the reporting host crontab just before the stat recomputation like this:
# 1. Copy script to reporting host in /usr/local/bin/
# 2. Verify the script is executable:
#    chmod +x /usr/local/bin/close-missed-long-calls.sh
# 3. Create a .pgpass file with the credentials to the xivo_stats database for asterisk on localhost
#    echo "localhost:5443:xivo_stats:asterisk:proformatique" >> /root/.pgpass
#    chmod 600 /root/.pgpass
# 3. Create a crontab calling the script at 23.50 a.m. (before 1.05 a.m.)
# 50 23 * * * root /usr/local/bin/close-missed-long-calls.sh -d 2

readonly progname="${0##*/}"

psql_cmd() {
    local req="${1}"; shift
    local log_prefix="${1}"; shift

    local psql_res=""

    psql_res=$(psql -h localhost -p 5443 -U asterisk -w xivo_stats -qtc "${req}")
    if [ -z "${psql_res}" ]; then
        echo "${log_prefix}: none"|logger -t "${progname}"
    else
        # WARN: The following works only if request returns only a single column result
        psql_res_inlined=$(echo "${psql_res}"|tr -d '\n')
        echo "${log_prefix} ids: ${psql_res_inlined}" |logger -t "${progname}"
    fi
}

get_non_terminated_cq_calls_id() {
    local table="call_on_queue"
    local req="SELECT id FROM ${table} WHERE answer_time IS NOT NULL AND hangup_time IS NULL AND answer_time < current_timestamp ORDER BY queue_time;"
    psql_cmd "${req}" "currently unclosed ${table}"
}

get_non_terminated_cd_calls_id() {
    local table="call_data"
    local req="SELECT id FROM ${table} WHERE answer_time IS NOT NULL AND end_time IS NULL AND answer_time < current_timestamp ORDER BY start_time;"
    psql_cmd "${req}" "currently unclosed ${table}"
}

terminate_longcalls_callonqueue() {
    local duration=${1}; shift
    local table="call_on_queue"

    psql_cmd "UPDATE ${table}
                SET hangup_time=answer_time
                WHERE answer_time IS NOT NULL
                    AND answer_time < current_timestamp - interval '${duration} hours'
                    AND hangup_time IS NULL
                RETURNING id;" "closing ${table}"
}

terminate_longcalls_calldata() {
    local duration=${1}; shift
    local table="call_data"

    psql_cmd "UPDATE ${table}
                SET end_time=answer_time
                WHERE answer_time IS NOT NULL
                    AND answer_time < current_timestamp - interval '${duration} hours'
                    AND end_time IS NULL
                RETURNING id;" "closing ${table}"
}


usage() {
    echo "${progname} -d DURATION"
    echo -e "This script force closes calls fdor which we missed the hangup."
    echo -e "Note: this script needs a valid .pgpass file to be able to connect to db as asterisk user"
    echo -e "Updated call_data and call_on_queue ids will be printed in syslog"
    echo -e "\t - DURATION in hour of an unclosed call"
    echo -e "\t     defaults to 4 hours"
}

exit_abnormally() {
    usage
    exit 1
}

main() {
    if [ ! -f "${HOME}/.pgpass" ]; then
        echo "ERROR: ${progname} needs a valid .pgpass file to work and to be able to connect to db as asterisk user"
        exit_abnormally
    fi
    if [[ $# -eq 0 ]]; then
        echo "ERROR: ${progname} needs at least argument -d DURATION"
        exit_abnormally
    fi

    # Script defaults
    local duration=4
    local expected_opts=":d:"
    while getopts ${expected_opts} opt; do
        case "${opt}" in
            d)
                duration="${OPTARG}"
            ;;
            *)
            ;; 
        esac
    done

    get_non_terminated_cq_calls_id
    get_non_terminated_cd_calls_id

    terminate_longcalls_callonqueue "${duration}"
    terminate_longcalls_calldata "${duration}"
}

main "${@}"