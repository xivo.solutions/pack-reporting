#!/bin/bash

# This program is to be laucnhed in a crontab on host of the reporting server
# It is intended to fix call_on_queue hangup_time (old model) from xc_queue_call end_time (new model) :
#  it sets the call_on_queue.hangup_time to the xc_queue_call.end_time
#  when found (i.e. for this the call_on_queue.callid and xc_queue_call.unique_id must match
#  which may not be always the case)

# This program can be put in the reporting host crontab like this:
# 1. Copy script to reporting host in /usr/local/bin/
# 2. Verify the script is executable:
#    chmod +x /usr/local/bin/fix-cq-hangup-time.sh
# 3. Create a .pgpass file with the credentials to the xivo_stats database for asterisk on localhost
#    echo "localhost:5443:xivo_stats:asterisk:proformatique" >> /root/.pgpass
#    chmod 600 /root/.pgpass
# 3. Create a crontab to call the script every 15min (before specific-stat is launched)
# */15 * * * * root /usr/local/bin/fix-cq-hangup-time.sh

readonly progname="${0##*/}"

psql_cmd() {
    local req="${1}"; shift

    local psql_res=""

    psql_res=$(psql -h localhost -p 5443 -U asterisk -w xivo_stats -qtc "${req}")
    if [ -z "${psql_res}" ]; then
        echo "no call_on_queue updated"|logger -t "${progname}"
    else
        # Using while loop instead of for (see SC2066)
        echo "${psql_res}" | while IFS= read -r s; do 
            echo "${s}"|awk -F'|' '{print "call_on_queue id "$1" updated with end_time "$3" from xc_queue_call id "$2}'|logger -t "${progname}"
        done
    fi
}

fix_cq_hanguptime_with_xc_qc_endtime() {

    psql_cmd "UPDATE call_on_queue cq
              SET hangup_time=xc_qc.end_time
              FROM xc_queue_call xc_qc
              WHERE xc_qc.unique_id=cq.callid
                AND cq.answer_time >= current_timestamp - interval '1 day'
                AND cq.hangup_time IS NULL
                AND xc_qc.start_time >= current_timestamp - interval '1 day'
                AND xc_qc.end_time IS NOT NULL
                RETURNING cq.id,xc_qc.id,xc_qc.end_time;"
}

usage() {
    echo "${progname}"
    echo -e "This script force closes call_on_queue calls with xc_queue_call end_time."
    echo -e "Updated call_on_queue entries will be printed in syslog with the corresponding xc_queue_call"
    echo -e "  grep ${progname} /var/log/syslog to see them"
    echo -e "Note: this script needs a valid .pgpass file to be able to connect to db as asterisk user"
}

exit_abnormally() {
    usage
    exit 1
}

main() {
    if [ ! -f "${HOME}/.pgpass" ]; then
        echo "ERROR: ${progname} needs a valid .pgpass file to work and to be able to connect to db as asterisk user"
        exit_abnormally
    fi

    fix_cq_hanguptime_with_xc_qc_endtime
}

main "${@}"