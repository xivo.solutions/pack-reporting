#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from distutils.core import setup
from pack_reporting.version import VERSION

setup(
    name='pack-reporting',
    version=VERSION,
    description='Pack reporting for the XiVO statistics server',
    author='Avencall',
    author_email='services@avencall.com',
    license='GPLv3',
    packages=['pack_reporting'],
    scripts=['bin/purge-db',
             'bin/specific-stats']
)
