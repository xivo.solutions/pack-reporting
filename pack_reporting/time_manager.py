# -*- coding: utf-8 -*-

# Copyright (C) 2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses>

from datetime import timedelta, datetime
import logging
import sys


logger = logging.getLogger(__name__)


class TimeManager:

    def __init__(self, period):
        self.period = period

    def get_queues_start_date(self, session):
        try:
            max_time = session.execute('SELECT max("time") AS max_time FROM stat_queue_specific').first()[0]
        except Exception:
            logger.exception('Error looking for start date in stat_queue_specific, exiting')
            session.close()
            sys.exit(1)
        if max_time is None:
            return self.get_start_date_from_call_on_queue(session)
        else:
            return max_time + self.period

    def get_agents_start_date(self, session):
        try:
            max_time = session.execute('SELECT max("time") AS max_time FROM stat_agent_specific').first()[0]
        except Exception:
            logger.exception('Error looking for start date in stat_agent_specific, exiting')
            session.close()
            sys.exit(1)
        if max_time is None:
            logger.info('No agent stat previously generated, looking for start date in queue_log')
            return self.get_start_date_from_call_on_queue(session)
        else:
            return max_time + self.period

    def get_agents_queue_start_date(self, session):
        try:
            max_time = session.execute('SELECT max("time") AS max_time FROM stat_agent_queue_specific').first()[0]
        except Exception:
            logger.exception('Error looking for start date in stat_agent_queue_specific, exiting')
            session.close()
            sys.exit(1)
        if max_time is None:
            logger.info('No agent queue stat previously generated, looking for start date in call_on_queue')
            return self.get_start_date_from_call_on_queue(session)
        else:
            return max_time + self.period

    def get_start_date_from_call_on_queue(self, session):
        start_date = session.execute("SELECT min(queue_time) FROM call_on_queue").first()[0]
        if start_date is None:
            logger.exception('Error looking for start date in call_on_queue')
            session.close()
            sys.exit(1)
        return self.round_to_interval(datetime(start_date.year, start_date.month, start_date.day, start_date.hour, start_date.minute, start_date.second))

    def get_global_end_date(self, session):
        end_date = session.execute("SELECT CAST(max(time) AS timestamp) FROM queue_log").first()[0]
        if end_date is None:
            logger.exception('Error looking for start date in queue_log')
            session.close()
            sys.exit(1)
        return self.round_to_interval(datetime(end_date.year, end_date.month, end_date.day, end_date.hour, end_date.minute, end_date.second))

    def round_to_interval(self, date):
        return date - (timedelta(seconds=(date - datetime(year=1970, month=1, day=1)).total_seconds() % self.period.total_seconds()))

    def split_interval(self, start, end):
        res = []
        local_start = start
        local_end = start + self.period
        while local_end <= end:
            res.append((local_start, local_end))
            local_start = local_end
            local_end = local_start + self.period
        return res

    @classmethod
    def date_from_string(cls, string):
        return datetime.strptime(string, '%Y-%m-%d %H:%M:%S')
