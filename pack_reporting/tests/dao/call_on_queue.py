# -*- coding: utf-8 -*-

# Copyright (C) 2012-2014 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from sqlalchemy.schema import Column, Table
from sqlalchemy.types import String, TIMESTAMP, Integer, Enum
from pack_reporting.tests.dao import metadata


CallOnQueue = Table('call_on_queue', metadata, Column('id', Integer, primary_key=True),
                                               Column('callid', String, nullable=False),
                                               Column('queue_time', TIMESTAMP),
                                               Column('total_ring_seconds', Integer),
                                               Column('answer_time', TIMESTAMP),
                                               Column('hangup_time', TIMESTAMP),
                                               Column('status', Enum('full',
                                                                     'closed',
                                                                     'joinempty',
                                                                     'leaveempty',
                                                                     'divert_ca_ratio',
                                                                     'divert_waittime',
                                                                     'answered',
                                                                     'abandoned',
                                                                     'timeout',
                                                                     name='call_exit_type')),
                                               Column('queue_ref', String),
                                               Column('agent_num', String))

XcQueueCall = Table('xc_queue_call', metadata, Column('id', Integer, primary_key=True),
                                               Column('queue_ref', String),
                                               Column('start_time', TIMESTAMP),
                                               Column('end_time', TIMESTAMP),
                                               Column('answer_time', TIMESTAMP),
                                               Column('ring_duration', Integer),
                                               Column('termination', Enum('full',
                                                                          'closed',
                                                                          'joinempty',
                                                                          'leaveempty',
                                                                          'divert_ca_ratio',
                                                                          'divert_waittime',
                                                                          'answered',
                                                                          'abandoned',
                                                                          'timeout',
                                                                          'exit_with_key',
                                                                          'system_hangup'
                                                                          'stalled',
                                                                          name='queue_termination_type')),
                                               Column('agent_id', Integer),
                                               Column('unique_id', String, nullable=False))
