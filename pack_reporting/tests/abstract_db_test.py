# -*- coding: utf-8 -*-
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
import unittest


class AbstractDbTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        engine = create_engine('postgresql://asterisk:proformatique@db/xivo_stats')
        cls.maker = sessionmaker(bind=engine)
        cls.session = cls.maker()

    @classmethod
    def tearDownClass(cls):
        cls.session.close()
