# -*- coding: utf-8 -*-
from datetime import datetime
from mock import Mock, patch, call
from pack_reporting.tests.dao.call_data import CallData
from pack_reporting.tests.dao.queue_log import QueueLog
from pack_reporting.tests.dao.call_on_queue import CallOnQueue, XcQueueCall
from pack_reporting import additional_stats
from pack_reporting.tests.abstract_db_test import AbstractDbTest
from pack_reporting.tests.dao.hold_periods import HoldPeriod


class TestStatsSpecifiques(AbstractDbTest):

    def setUp(self):
        self.session.execute('''TRUNCATE call_on_queue, queue_log, stat_queue_specific, stat_agent_specific, call_data, stat_agent_queue_specific,
        hold_periods, queue_threshold_time CASCADE''')
        self.session.commit()

    def insert_stat_data(self, statData):

        def insert_call_on_queue(data):
            self.session.execute(CallOnQueue.insert(values=data))

        def insert_xc_queue_call(data):
            self.session.execute(XcQueueCall.insert(values=data))

        def insert_queue_log(data):
            self.session.execute(QueueLog.insert(values=data))

        def insert_call_data(data):
            self.session.execute(CallData.insert(values=data))

        def insert_hold_period(data):
            self.session.execute(HoldPeriod.insert(values=data))

        statDataMethods = {'callOnQueue': insert_call_on_queue,
                           'queueLog': insert_queue_log,
                           'callData': insert_call_data,
                           'holdPeriod': insert_hold_period,
                           'xcQueueCall': insert_xc_queue_call
        }

        for (k, v) in statData.items():
            statDataMethods[k](v)

    def test_insert_stat_queue_specific(self):
        call_previously_answered = {
            'callData': {'uniqueid': '12345.677', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:28:00',
                         'answer_time': '2012-01-01 10:28:30', 'end_time': '2012-01-01 10:30:30', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.677', 'queue_time': '2012-01-01 10:28:00', 'answer_time': '2012-01-01 10:28:30', 'hangup_time': '2012-01-01 10:30:30',
                            'status': 'answered', 'queue_ref': 'thequeue'}
        }

        first_call_answered = {
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:01',
                         'answer_time': '2012-01-01 10:33:18', 'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:20', 'hangup_time': '2012-01-01 10:33:50',
                            'status': 'answered', 'queue_ref': 'thequeue'},
            'queueLog': {'time': '2012-01-01 10:33:18', 'callid': '12345.678', 'queuename': 'thequeue', 'event': 'WRAPUPSTART', 'data1': '10'}
        }

        second_call_answered = {
            'callData': {'uniqueid': '12345.688', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:34:00',
                         'answer_time': '2012-01-01 10:34:11', 'end_time': '2012-01-01 10:47:30', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.688', 'queue_time': '2012-01-01 10:40:00', 'answer_time': '2012-01-01 10:40:10', 'hangup_time': '2012-01-01 10:47:10',
                            'status': 'answered', 'queue_ref': 'thequeue'},
            'queueLog': {'time': '2012-01-01 10:47:10', 'callid': '12345.688', 'queuename': 'thequeue', 'event': 'WRAPUPSTART', 'data1': '10'}
        }

        call_cancelled = {
            'callData': {'uniqueid': '12345.679', 'dst_num': '4001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:35:00',
                         'answer_time': None, 'end_time': '2012-01-01 10:36:00', 'status': None},
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:35:00', 'hangup_time': '2012-01-01 10:36:00', 'status': 'abandoned', 'queue_ref': 'thequeue'},
            'queueLog': {'callid': '12345.688', 'event': 'TEST', 'time': '2012-01-01 10:39:59', 'data1': 12}
        }

        call_cancelled_started_previous_period = {
            'callData': {'uniqueid': '12345.888', 'dst_num': '4001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:29:00',
                         'answer_time': None, 'end_time': '2012-01-01 10:31:00', 'status': None},
            'callOnQueue': {'callid': '12345.888', 'queue_time': '2012-01-01 10:29:00', 'hangup_time': '2012-01-01 10:31:00', 'status': 'abandoned', 'queue_ref': 'thequeue'}
        }

        call_waiting_and_answered_in_the_next_15_minutes = {
            'callData': {'uniqueid': '12345.680', 'dst_num': '4001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:00',
                         'answer_time': '2012-01-01 10:45:15', 'end_time': '2012-01-01 10:45:30', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.680', 'queue_time': '2012-01-01 10:44:00', 'answer_time': '2012-01-01 10:45:15', 'hangup_time': '2012-01-01 10:45:30', 'status': 'answered', 'queue_ref': 'thequeue'}
        }

        self.insert_stat_data(call_previously_answered)
        self.insert_stat_data(first_call_answered)
        self.insert_stat_data(second_call_answered)
        self.insert_stat_data(call_cancelled)
        self.insert_stat_data(call_cancelled_started_previous_period)
        self.insert_stat_data(call_waiting_and_answered_in_the_next_15_minutes)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_queue_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_queue_specific WHERE time = \'2012-01-01 10:30:00\'')
        length = 0
        for result in results:
            length += 1
            if result['dst_num'] == '4000':
                self.assertEquals('thequeue', result['queue_ref'])
                self.assertEquals(2, result['nb_offered'])
                self.assertEquals(30, result['sum_resp_delay'])
                self.assertEquals(1, result['answer_btw_t1_t2'])
                self.assertEquals(1, result['answer_less_t1'])
                self.assertEquals(350, result['communication_time'])
                self.assertEquals(10, result['wrapup_time'])
            else:
                self.assertEquals('thequeue', result['queue_ref'])
                self.assertEquals('4001', result['dst_num'])
                self.assertEquals(2, result['nb_offered'])
                self.assertEquals(2, result['nb_abandoned'])
                self.assertEquals(2, result['abandoned_more_t2'])
                self.assertIsNone(result['communication_time'])
                self.assertEquals(75, result['sum_resp_delay'])
                self.assertIsNone(result['answer_btw_t1_t2'])
                self.assertIsNone(result['answer_less_t1'])
                self.assertIsNone(result['wrapup_time'])
        self.assertEquals(2, length)

        start_date2 = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        end_date2 = datetime.strptime('2012-01-01 11:00:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_queue_specific(self.session, start_date2, end_date2)

        results = self.session.execute('SELECT * FROM stat_queue_specific WHERE time = \'2012-01-01 10:45:00\'')
        length = 0
        for result in results:
            length += 1
            if result['dst_num'] == '4001':
                self.assertEquals('thequeue', result['queue_ref'])
                self.assertIsNone(result['sum_resp_delay'])
                self.assertEquals(0, result['answer_btw_t1_t2'])
                self.assertEquals(0, result['answer_less_t1'])
                self.assertEquals(15, result['communication_time'])
                self.assertIsNone(result['wrapup_time'])
            else:
                self.assertEquals('4000', result['dst_num'])
                self.assertEquals('thequeue', result['queue_ref'])
                self.assertIsNone(result['sum_resp_delay'])
                self.assertEquals(0, result['answer_btw_t1_t2'])
                self.assertEquals(0, result['answer_less_t1'])
                self.assertEquals(130, result['communication_time'])
                self.assertEquals(10, result['wrapup_time'])

        self.assertEquals(2, length)

    def test_insert_stat_queue_specific_pending_calls(self):
        first_answered_call = {
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:29:00',
                         'answer_time': '2012-01-01 10:29:30', 'end_time': None, 'status': 'answer'},
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:29:00', 'answer_time': '2012-01-01 10:29:30', 'hangup_time': None,
                            'status': 'answered', 'queue_ref': 'thequeue'}
        }

        second_answered_call = {
            'callData': {'uniqueid': '12345.679', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:43:00',
                         'answer_time': '2012-01-01 10:43:30', 'end_time': None, 'status': 'answer'},
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:43:00', 'answer_time': '2012-01-01 10:43:30', 'hangup_time': None,
                            'status': 'answered', 'queue_ref': 'thequeue'}
        }

        call_not_yet_answered = {
            'callData': {'uniqueid': '12345.682', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:43:00',
                         'answer_time': None, 'end_time': None, 'status': None},
            'callOnQueue': {'callid': '12345.682', 'queue_time': '2012-01-01 10:43:00', 'answer_time': None, 'hangup_time': None,
                            'status': None, 'queue_ref': 'thequeue'}
        }

        call_out_of_considered_period = {
            'callData': [{'uniqueid': '12345.680', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:46:00',
                          'answer_time': None, 'end_time': None, 'status': 'answer'},
                         {'uniqueid': '12345.681', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:28:00',
                          'answer_time': '2012-01-01 10:28:30', 'end_time': '2012-01-01 10:29:00', 'status': 'answer'}],
            'callOnQueue': [{'callid': '12345.680', 'queue_time': '2012-01-01 10:46:00', 'answer_time': None, 'hangup_time': None,
                             'status': None, 'queue_ref': 'thequeue'},
                            {'callid': '12345.681', 'queue_time': '2012-01-01 10:28:00', 'answer_time': '2012-01-01 10:28:30', 'hangup_time': '2012-01-01 10:29:00',
                             'status': 'answered', 'queue_ref': 'thequeue'}]
        }

        self.insert_stat_data(first_answered_call)
        self.insert_stat_data(second_answered_call)
        self.insert_stat_data(call_not_yet_answered)
        self.insert_stat_data(call_out_of_considered_period)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_queue_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_queue_specific')
        length = 0
        for result in results:
            length += 1
            self.assertEquals(result['dst_num'], '4000')
            self.assertEquals(result['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
            self.assertEquals('thequeue', result['queue_ref'])
            self.assertEquals(2, result['nb_offered'])
            self.assertEquals(30, result['sum_resp_delay'])
            self.assertEquals(0, result['answer_btw_t1_t2'])
            self.assertEquals(0, result['answer_less_t1'])
            self.assertEquals(990, result['communication_time'])
        self.assertEquals(1, length)

    def test_insert_stat_queue_specific_specific_thresholds(self):
        # t1 = 20, t2 = 30
        call_answered_less_t1 = {
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:01',
                         'answer_time': '2012-01-01 10:33:20', 'end_time': '2012-01-01 10:33:47', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:01', 'answer_time': '2012-01-01 10:33:20',
                            'hangup_time': '2012-01-01 10:33:47', 'status': 'answered', 'queue_ref': 'thequeue'}
        }

        call_answered_btw_t1_t2 = {
            'callData': {'uniqueid': '12345.688', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:34:00',
                         'answer_time': '2012-01-01 10:34:25', 'end_time': '2012-01-01 10:47:35', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.688', 'queue_time': '2012-01-01 10:34:00', 'answer_time': '2012-01-01 10:34:25', 'hangup_time': '2012-01-01 10:47:10',
                            'status': 'answered', 'queue_ref': 'thequeue'}
        }

        call_abandoned_btw_t1_t2 = {
            'callData': {'uniqueid': '12345.679', 'dst_num': '4001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:35:00',
                         'answer_time': None, 'end_time': '2012-01-01 10:35:25', 'status': None},
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:35:00', 'hangup_time': '2012-01-01 10:35:25',
                            'status': 'abandoned', 'queue_ref': 'thequeue'}
        }

        call_abandoned_more_t2 = {
            'callData': {'uniqueid': '12345.888', 'dst_num': '4001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:36:00',
                         'answer_time': None, 'end_time': '2012-01-01 10:36:35', 'status': None},
            'callOnQueue': {'callid': '12345.888', 'queue_time': '2012-01-01 10:36:00', 'hangup_time': '2012-01-01 10:36:35',
                            'status': 'abandoned', 'queue_ref': 'thequeue'}
        }

        self.insert_stat_data(call_answered_less_t1)
        self.insert_stat_data(call_answered_btw_t1_t2)
        self.insert_stat_data(call_abandoned_btw_t1_t2)
        self.insert_stat_data(call_abandoned_more_t2)
        self.session.execute('INSERT INTO queue_threshold_time(queue_ref, t1, t2) VALUES (\'thequeue\', 20, 30)')
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_queue_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_queue_specific WHERE time = \'2012-01-01 10:30:00\'')
        length = 0
        for result in results:
            length += 1
            if result['dst_num'] == '4000':
                self.assertEquals('thequeue', result['queue_ref'])
                self.assertEquals(1, result['answer_btw_t1_t2'])
                self.assertEquals(1, result['answer_less_t1'])
                self.assertIsNone(result['abandoned_more_t2'])
                self.assertIsNone(result['abandoned_btw_t1_t2'])
            else:
                self.assertEquals('thequeue', result['queue_ref'])
                self.assertIsNone(result['answer_btw_t1_t2'])
                self.assertIsNone(result['answer_less_t1'])
                self.assertEquals(1, result['abandoned_more_t2'])
                self.assertEquals(1, result['abandoned_btw_t1_t2'])
        self.assertEquals(2, length)

    def test_insert_stat_queue_specific_hold_time(self):
        first_call_answered_and_held = {
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:01',
                         'answer_time': '2012-01-01 10:33:18', 'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:20', 'hangup_time': '2012-01-01 10:33:50',
                            'status': 'answered', 'queue_ref': 'thequeue'},
            'holdPeriod': {'linkedid': '12345.678', 'start': '2012-01-01 10:33:30', 'end': '2012-01-01 10:33:40'}
        }

        second_call_answered_and_held = {
            'callData': {'uniqueid': '12345.679', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:43:00',
                         'answer_time': '2012-01-01 10:43:20', 'end_time': '2012-01-01 10:47:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:43:00', 'answer_time': '2012-01-01 10:43:20', 'hangup_time': '2012-01-01 10:47:43',
                            'status': 'answered', 'queue_ref': 'thequeue'},
            'holdPeriod': {'linkedid': '12345.679', 'start': '2012-01-01 10:44:50', 'end': '2012-01-01 10:45:10'}
        }

        third_call_answered_and_held = {
            'callData': {'uniqueid': '12345.680', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:25:00',
                         'answer_time': '2012-01-01 10:25:20', 'end_time': '2012-01-01 10:35:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.680', 'queue_time': '2012-01-01 10:25:00', 'answer_time': '2012-01-01 10:25:20', 'hangup_time': '2012-01-01 10:35:43',
                            'status': 'answered', 'queue_ref': 'thequeue'},
            'holdPeriod': {'linkedid': '12345.680', 'start': '2012-01-01 10:25:30', 'end': '2012-01-01 10:25:40'}
        }

        self.insert_stat_data(first_call_answered_and_held)
        self.insert_stat_data(second_call_answered_and_held)
        self.insert_stat_data(third_call_answered_and_held)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_queue_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_queue_specific')
        length = 0
        for result in results:
            length += 1
            self.assertEquals(result['dst_num'], '4000')
            self.assertEquals(result['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
            self.assertEquals('thequeue', result['queue_ref'])
            self.assertEquals(20, result['hold_time'])
        self.assertEquals(1, length)

    def test_insert_stat_queue_specific_dissuaded(self):
        call_timeout = {
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'end_time': '2012-01-01 10:33:50', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'hangup_time': '2012-01-01 10:33:50',
                            'status': 'timeout', 'queue_ref': 'thequeue'}
        }

        call_leaveempty = {
            'callData': {'uniqueid': '12345.679', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'end_time': '2012-01-01 10:33:50', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:33:00', 'hangup_time': '2012-01-01 10:33:43',
                            'status': 'leaveempty', 'queue_ref': 'thequeue'}
        }

        call_divert_ca_ratio = {
            'callData': {'uniqueid': '12345.680', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.680', 'queue_time': '2012-01-01 10:33:00', 'hangup_time': '2012-01-01 10:33:43',
                            'status': 'divert_ca_ratio', 'queue_ref': 'thequeue'}
        }

        call_divert_waittime = {
            'callData': {'uniqueid': '12345.681', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.681', 'queue_time': '2012-01-01 10:33:00', 'hangup_time': '2012-01-01 10:33:43',
                            'status': 'divert_waittime', 'queue_ref': 'thequeue'}
        }

        call_full = {
            'callData': {'uniqueid': '12345.682', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.682', 'queue_time': '2012-01-01 10:33:00', 'hangup_time': '2012-01-01 10:33:43',
                            'status': 'full', 'queue_ref': 'thequeue'}
        }

        call_closed = {
            'callData': {'uniqueid': '12345.683', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:25:00',
                         'end_time': '2012-01-01 10:35:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.683', 'queue_time': '2012-01-01 10:25:00', 'hangup_time': '2012-01-01 10:35:43',
                            'status': 'closed', 'queue_ref': 'thequeue'}
        }

        call_joinempty = {
            'callData': {'uniqueid': '12345.684', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:25:00',
                         'end_time': '2012-01-01 10:35:43', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.684', 'queue_time': '2012-01-01 10:25:00', 'hangup_time': '2012-01-01 10:35:43',
                            'status': 'joinempty', 'queue_ref': 'thequeue'}
        }

        call_keyexit = {
            'callData': {'uniqueid': '12345.685', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'end_time': '2012-01-01 10:33:50', 'status': 'answer'},
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'hangup_time': '2012-01-01 10:33:50',
                            'status': 'exit_with_key', 'queue_ref': 'thequeue'}
        }

        self.insert_stat_data(call_timeout)
        self.insert_stat_data(call_leaveempty)
        self.insert_stat_data(call_divert_ca_ratio)
        self.insert_stat_data(call_divert_waittime)
        self.insert_stat_data(call_full)
        self.insert_stat_data(call_closed)
        self.insert_stat_data(call_joinempty)
        self.insert_stat_data(call_keyexit)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_queue_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_queue_specific')
        length = 0
        for result in results:
            length += 1
            self.assertEquals(result['dst_num'], '4000')
            self.assertEquals(result['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
            self.assertEquals('thequeue', result['queue_ref'])
            self.assertEquals(3, result['nb_offered'])
        self.assertEquals(1, length)

    def test_insert_stat_agent_specific(self):
        call_answered_then_transfered_to_an_internal_number = {
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:18',
                           'hangup_time': '2012-01-01 10:33:43', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
             'queueLog': [{'time': '2012-01-01 10:33:18', 'callid': '12345.678', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'CONNECT'},
                          {'time': '2012-01-01 10:33:43', 'callid': '12345.678', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'TRANSFER'}],
             'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                          'answer_time': '2012-01-01 10:33:18', 'end_time': '2012-01-01 10:35:00', 'status': 'answer', 'ring_duration_on_answer': 5,
                          'transfered': True, 'transfer_direction': 'internal'}
            }

        call_answered = {
            'callOnQueue': {'callid': '12345.688', 'queue_time': '2012-01-01 10:34:00', 'answer_time': '2012-01-01 10:34:10',
                            'hangup_time': '2012-01-01 10:34:29', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'queueLog': {'time': '2012-01-01 10:34:10', 'callid': '12345.688', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'CONNECT'},
            'callData': {'uniqueid': '12345.688', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:34:00',
                         'answer_time': '2012-01-01 10:34:10', 'end_time': '2012-01-01 10:34:29', 'status': 'answer', 'ring_duration_on_answer': 7}
        }

        call_not_answered_and_canceled = {
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:35:00', 'answer_time': None,
                            'hangup_time': '2012-01-01 10:35:35', 'status': 'abandoned', 'queue_ref': 'thequeue'},
            'queueLog': {'time': '2012-01-01 10:35:01', 'callid': '12345.679', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'RINGNOANSWER', 'data1': '15000'},
            'callData': {'uniqueid': '12345.679', 'dst_num': '4000', 'call_direction': 'incoming', 'start_time': '2012-01-01 10:35:00',
                         'answer_time': None, 'end_time': '2012-01-01 10:35:35', 'status': None}
        }

        first_call_answered_and_transfered_to_an_external_number = {
            'callOnQueue': {'callid': '12345.700', 'queue_time': '2012-01-01 10:28:00', 'answer_time': '2012-01-01 10:29:00',
                            'hangup_time': '2012-01-01 10:31:00', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'queueLog': [{'time': '2012-01-01 10:29:00', 'callid': '12345.700', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'CONNECT'},
                         {'time': '2012-01-01 10:31:00', 'callid': '12345.700', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'TRANSFER'}],
            'callData': {'uniqueid': '12345.700', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:28:00',
                        'answer_time': '2012-01-01 10:29:00', 'end_time': '2012-01-01 10:33:00', 'status': 'answer', 'ring_duration_on_answer': 5,
                        'transfered': True, 'transfer_direction': 'outgoing'}
        }

        second_call_answered_and_transfered_to_an_external_number = {
            'callOnQueue': {'callid': '12345.701', 'queue_time': '2012-01-01 10:44:00', 'answer_time': '2012-01-01 10:44:17',
                            'hangup_time': '2012-01-01 10:49:17', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'queueLog': [{'time': '2012-01-01 10:44:17', 'callid': '12345.701', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'CONNECT'},
                         {'time': '2012-01-01 10:49:17', 'callid': '12345.701', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'TRANSFER'}],
            'callData': {'uniqueid': '12345.701', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:00',
                         'answer_time': '2012-01-01 10:44:17', 'end_time': '2012-01-01 11:53:27', 'status': 'answer', 'ring_duration_on_answer': 5,
                         'transfered': True, 'transfer_direction': 'outgoing'}
        }

        call_answered_in_the_next_15_minutes = {
            'callOnQueue': {'callid': '12345.702', 'queue_time': '2012-01-01 10:44:30', 'answer_time': '2012-01-01 10:45:30',
                           'hangup_time': '2012-01-01 10:46:30', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4001'},
            'queueLog': {'time': '2012-01-01 10:45:30', 'callid': '12345.702', 'queuename': 'thequeue', 'agent': 'Agent/4001', 'event': 'CONNECT'},
            'callData': {'uniqueid': '12345.702', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:30',
                         'answer_time': '2012-01-01 10:45:30', 'end_time': '2012-01-01 10:46:30', 'status': 'answer', 'ring_duration_on_answer': 7}
        }

        # outgoing calls through queues should not be seen as offered or answered
        outgoing_call_through_queue = {
            'callOnQueue': {'callid': '12345.703', 'queue_time': '2012-01-01 10:34:30', 'answer_time': '2012-01-01 10:34:30',
                           'hangup_time': '2012-01-01 10:40:30', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'queueLog': {'time': '2012-01-01 10:34:30', 'callid': '12345.703', 'queuename': 'thequeue', 'agent': 'Agent/4002', 'event': 'CONNECT'},
            'callData': {'uniqueid': '12345.703', 'dst_num': '4000', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:34:30',
                         'answer_time': '2012-01-01 10:35:00', 'end_time': '2012-01-01 10:40:30', 'status': 'answer', 'ring_duration_on_answer': 7}
        }

        self.insert_stat_data(call_answered_then_transfered_to_an_internal_number)
        self.insert_stat_data(call_answered)
        self.insert_stat_data(call_not_answered_and_canceled)
        self.insert_stat_data(first_call_answered_and_transfered_to_an_external_number)
        self.insert_stat_data(second_call_answered_and_transfered_to_an_external_number)
        self.insert_stat_data(call_answered_in_the_next_15_minutes)
        self.insert_stat_data(outgoing_call_through_queue)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_agent_specific')
        length = 0
        for result in results:
            length += 1
            self.assertEquals(result['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
            self.assertEquals(result['agent_num'], '4002')
            self.assertEquals(5, result['nb_offered'])
            self.assertEquals(4, result['nb_answered'])
            self.assertEquals(147, result['conversation_time'])
            self.assertEquals(32, result['ringing_time'])
            self.assertEquals(1, result['nb_transfered_intern'])
            self.assertEquals(1, result['nb_transfered_extern'])

        self.assertEquals(1, length)

    def test_insert_stat_agent_specific_hold_time(self):
        first_call_answered_and_held = {
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:18',
                            'hangup_time': '2012-01-01 10:33:43', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.678', 'src_num': '1000', 'call_direction': 'incoming',
                         'start_time': '2012-01-01 10:33:00',
                         'answer_time': '2012-01-01 10:33:18',
                         'end_time': '2012-01-01 10:33:43',
                         'status': 'answer',
                         'dst_agent': '4002'},
            'holdPeriod': {'linkedid': '12345.678', 'start': '2012-01-01 10:33:30', 'end': '2012-01-01 10:33:40'}
        }

        second_call_answered_and_held = {
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:44:00', 'answer_time': '2012-01-01 10:44:18',
                            'hangup_time': '2012-01-01 10:47:40', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.679', 'src_num': '1000', 'call_direction': 'incoming',
                         'start_time': '2012-01-01 10:44:00',
                         'answer_time': '2012-01-01 10:44:18',
                         'end_time': '2012-01-01 10:47:40',
                         'status': 'answer',
                         'dst_agent': '4002'},
            'holdPeriod': {'linkedid': '12345.679', 'start': '2012-01-01 10:44:50', 'end': '2012-01-01 10:45:10'}
        }

        third_call_answered_and_held = {
            'callOnQueue': {'callid': '12345.680', 'queue_time': '2012-01-01 10:27:00', 'answer_time': '2012-01-01 10:27:18',
                            'hangup_time': '2012-01-01 10:32:43', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.680', 'src_num': '1000', 'call_direction': 'incoming',
                         'start_time': '2012-01-01 10:27:00',
                         'answer_time': '2012-01-01 10:27:18',
                         'end_time': '2012-01-01 10:32:43',
                         'status': 'answer',
                         'dst_agent': '4002'},
            'holdPeriod': {'linkedid': '12345.680', 'start': '2012-01-01 10:28:30', 'end': '2012-01-01 10:28:40'}
        }

        self.insert_stat_data(first_call_answered_and_held)
        self.insert_stat_data(second_call_answered_and_held)
        self.insert_stat_data(third_call_answered_and_held)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_agent_specific')
        length = 0
        for result in results:
            length += 1
            self.assertEquals(result['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
            self.assertEquals(result['agent_num'], '4002')
            self.assertEquals(20, result['hold_time'])
        self.assertEquals(1, length)

    def test_stats_outgoing_calls(self):
        call_toExtern_still_connected = {'callData': {'uniqueid': '12345.235', 'src_num': '1000', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:43:50',
                                                      'answer_time': '2012-01-01 10:44:00', 'end_time': None, 'status': 'answer', 'src_agent': '4001'}}

        call_toExtern_1min_long_agent4001 = {'callData': {'uniqueid': '12345.236', 'src_num': '1000', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:30:50',
                                                     'answer_time': '2012-01-01 10:30:57', 'end_time': '2012-01-01 10:31:57', 'status': 'answer', 'src_agent': '4001'}}

        call_toExtern_1min_long_agent4002 = {'callData': {'uniqueid': '12345.237', 'src_num': '1001', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:30:50',
                                                     'answer_time': '2012-01-01 10:30:57', 'end_time': '2012-01-01 10:31:57', 'status': 'answer', 'src_agent': '4002'}}

        calls_toExtern_out_of_considered_period = {'callData': [{'uniqueid': '12345.238', 'src_num': '1001', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:20:50',
                                                                'answer_time': '2012-01-01 10:20:57', 'end_time': '2012-01-01 10:21:57', 'status': 'answer', 'src_agent': '4002'},
                                                                {'uniqueid': '12345.239', 'src_num': '1001', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:50:50',
                                                                 'answer_time': '2012-01-01 10:50:57', 'end_time': '2012-01-01 10:51:57', 'status': 'answer', 'src_agent': '4002'}]}

        call_toExtern_cancelled = {'callData': {'uniqueid': '12345.240', 'src_num': '1001', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:30:00',
                                                     'answer_time': None, 'end_time': '2012-01-01 10:30:30', 'status': None, 'src_agent': '4002'}}

        call_toExtern_answered_in_the_next_15min = {'callData': {'uniqueid': '12345.241', 'src_num': '1001', 'call_direction': 'outgoing', 'start_time': '2012-01-01 10:44:30',
                                                     'answer_time': '2012-01-01 10:45:30', 'end_time': '2012-01-01 10:46:30', 'status': 'answer', 'src_agent': '4002'}}

        self.insert_stat_data(call_toExtern_still_connected)
        self.insert_stat_data(call_toExtern_1min_long_agent4001)
        self.insert_stat_data(call_toExtern_1min_long_agent4002)
        self.insert_stat_data(calls_toExtern_out_of_considered_period)
        self.insert_stat_data(call_toExtern_cancelled)
        self.insert_stat_data(call_toExtern_answered_in_the_next_15min)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_specific(self.session, start_date, end_date)

        resultats = self.session.execute('SELECT * FROM stat_agent_specific')
        length = 0
        for resultat in resultats:
            length += 1
            if resultat['agent_num'] == '4001':
                self.assertEquals('2012-01-01 10:30:00', resultat['time'].strftime('%Y-%m-%d %H:%M:%S'))
                self.assertEquals(2, resultat['nb_outgoing_calls'])
                self.assertEquals(120, resultat['conversation_time_outgoing_calls'])
            elif resultat['agent_num'] == '4002':
                self.assertEquals('2012-01-01 10:30:00', resultat['time'].strftime('%Y-%m-%d %H:%M:%S'))
                self.assertEquals(3, resultat['nb_outgoing_calls'])
                self.assertEquals(60, resultat['conversation_time_outgoing_calls'])
            else:
                self.fail('Anormal')
        self.assertEquals(2, length)

    def test_stats_emitted_internal_calls(self):
        call_toExtern_2min_long_agent4001 = {'callData': {'uniqueid': '12345.235', 'src_num': '1000', 'dst_num': '2000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:40:50',
                                                     'answer_time': '2012-01-01 10:40:57', 'end_time': '2012-01-01 10:42:57', 'status': 'answer', 'src_agent': '4001'}}

        call_toExtern_1min_long_agent4001 = {'callData': {'uniqueid': '12345.236', 'src_num': '1000', 'dst_num': '2000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:30:50',
                                                     'answer_time': '2012-01-01 10:30:57', 'end_time': '2012-01-01 10:31:57', 'status': 'answer', 'src_agent': '4001'}}

        call_to_technical_number_not_being_considered = {'callData': {'uniqueid': '12345.245', 'src_num': '1000', 'dst_num': '*271000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:30:50',
                                                     'answer_time': '2012-01-01 10:30:57', 'end_time': '2012-01-01 10:31:57', 'status': 'answer', 'src_agent': '4001'}}

        call_toIntern_started_in_the_previous_15min_and_not_terminated_agent4002 = {
            'callData': {'uniqueid': '12345.237', 'src_num': '1001', 'dst_num': '2000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:28:50',
                        'answer_time': '2012-01-01 10:29:00', 'end_time': None, 'status': 'answer', 'src_agent': '4002'}}

        call_toExtern_canceled = {'callData': {'uniqueid': '12345.238', 'src_num': '1000', 'dst_num': '2000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:32:00',
                                                     'answer_time': None, 'end_time': '2012-01-01 10:32:30', 'status': None, 'src_agent': '4001'}}

        call_toExtern_answered_in_the_next_15min = {'callData': {'uniqueid': '12345.239', 'src_num': '1000', 'dst_num': '2000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:30',
                                                     'answer_time': '2012-01-01 10:45:30', 'end_time': '2012-01-01 10:46:30', 'status': 'answer', 'src_agent': '4001'}}

        self.insert_stat_data(call_toExtern_2min_long_agent4001)
        self.insert_stat_data(call_toExtern_1min_long_agent4001)
        self.insert_stat_data(call_to_technical_number_not_being_considered)
        self.insert_stat_data(call_toIntern_started_in_the_previous_15min_and_not_terminated_agent4002)
        self.insert_stat_data(call_toExtern_canceled)
        self.insert_stat_data(call_toExtern_answered_in_the_next_15min)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_specific(self.session, start_date, end_date)

        resultats = self.session.execute('SELECT * FROM stat_agent_specific')
        length = 0
        for resultat in resultats:
            length += 1
            if resultat['agent_num'] == '4001':
                self.assertEquals('2012-01-01 10:30:00', resultat['time'].strftime('%Y-%m-%d %H:%M:%S'))
                self.assertEquals(4, resultat['nb_emitted_internal_calls'])
                self.assertEquals(180, resultat['conversation_time_emitted_internal_calls'])
            elif resultat['agent_num'] == '4002':
                self.assertEquals('2012-01-01 10:30:00', resultat['time'].strftime('%Y-%m-%d %H:%M:%S'))
                self.assertEquals(None, resultat['nb_emitted_internal_calls'])
                self.assertEquals(900, resultat['conversation_time_emitted_internal_calls'])
            else:
                self.fail('Anormal')
        self.assertEquals(2, length)

    def test_clean_db(self):
        self.session.execute('INSERT INTO stat_agent_specific(time, agent_num) VALUES(:time, :agent_num)', {'time': '2012-01-01 00:00:00', 'agent_num': '4001'})
        self.session.execute('INSERT INTO stat_queue_specific(time, queue_ref) VALUES(:time, :queue_ref)', {'time': '2012-01-01 00:00:00', 'queue_ref': 'thequeue'})
        self.session.commit()

        additional_stats.clear_stats(self.session)

        nb_stat_agent_specific = self.session.execute('SELECT COUNT(*) FROM stat_agent_specific').first()[0]
        nb_stat_queue_specific = self.session.execute('SELECT COUNT(*) FROM stat_queue_specific').first()[0]
        self.assertEquals((0, 0), (nb_stat_agent_specific, nb_stat_queue_specific))

    def test_direct_agent_calls(self):
        calls_internals = {'callData': [
            {'uniqueid': '123.456', 'dst_num': '1000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:30:00',
             'answer_time': '2012-01-01 10:30:05', 'end_time': '2012-01-01 10:30:45', 'status': 'answer', 'dst_agent': None},
            {'uniqueid': '123.470', 'dst_num': '1000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:43:00',
             'answer_time': '2012-01-01 10:43:05', 'end_time': '2012-01-01 10:43:55', 'status': 'answer', 'dst_agent': '4001'},
            {'uniqueid': '123.457', 'dst_num': '1000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:00',
             'answer_time': '2012-01-01 10:44:05', 'end_time': '2012-01-01 10:47:05', 'status': 'answer', 'dst_agent': '4001'},
            {'uniqueid': '123.458', 'dst_num': '1000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:40:00',
             'end_time': '2012-01-01 10:41:00', 'status': None, 'answer_time': None, 'dst_agent': '4001'},
            {'uniqueid': '123.459', 'dst_num': '1001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:30:00',
             'answer_time': '2012-01-01 10:30:05', 'end_time': '2012-01-01 10:38:00', 'status': 'answer', 'dst_agent': '4002'}
        ]}

        call_answered_in_the_next_15min = {'callData':
                                               {'uniqueid': '123.460', 'dst_num': '1001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:30',
                                                'answer_time': '2012-01-01 10:45:30', 'end_time': '2012-01-01 10:46:30', 'status': 'answer', 'dst_agent': '4002'}}

        call_out_of_considered_period = {'callData':
                                             {'uniqueid': '123.498', 'dst_num': '1001', 'call_direction': 'internal', 'start_time': '2012-01-01 10:48:00',
                                              'answer_time': '2012-01-01 10:48:05', 'end_time': '2012-01-01 10:56:00', 'status': 'answer', 'dst_agent': '4002'}}

        incoming_calls = {'callData': [
            {'uniqueid': '123.469', 'dst_num': '51000', 'call_direction': 'incoming', 'start_time': '2012-01-01 10:41:00',
             'answer_time': '2012-01-01 10:41:05', 'end_time': '2012-01-01 10:43:00', 'status': 'answer', 'dst_agent': '4001'},
            {'uniqueid': '123.587', 'dst_num': '51000', 'call_direction': 'incoming', 'start_time': '2012-01-01 10:44:00',
             'answer_time': '2012-01-01 10:44:05', 'end_time': None, 'status': 'answer', 'dst_agent': '4001'},
            {'uniqueid': '123.588', 'dst_num': '51000', 'call_direction': 'incoming', 'start_time': '2012-01-01 10:44:30',
             'answer_time': '2012-01-01 10:45:30', 'end_time': '2012-01-01 10:46:30', 'status': 'answer', 'dst_agent': '4001'}
        ]}

        self.insert_stat_data(call_answered_in_the_next_15min)
        self.insert_stat_data(call_out_of_considered_period)
        self.insert_stat_data(incoming_calls)
        self.insert_stat_data(calls_internals)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_specific(self.session, start_date, end_date)

        resultats = self.session.execute('SELECT * FROM stat_agent_specific')
        length = 0
        for resultat in resultats:
            length += 1
            if resultat['agent_num'] == '4001' and resultat['time'].strftime('%Y-%m-%d %H:%M:%S') == '2012-01-01 10:30:00':
                self.assertEquals(3, resultat['nb_incoming_calls'])
                self.assertEquals(170, resultat['conversation_time_incoming_calls'])
                self.assertEquals(3, resultat['nb_received_internal_calls'])
                self.assertEquals(105, resultat['conversation_time_received_internal_calls'])
            elif resultat['agent_num'] == '4002':
                self.assertEquals('2012-01-01 10:30:00', resultat['time'].strftime('%Y-%m-%d %H:%M:%S'))
                self.assertEquals(2, resultat['nb_received_internal_calls'])
                self.assertEquals(475, resultat['conversation_time_received_internal_calls'])
            else:
                self.fail('Anormal')
        self.assertEquals(2, length)

    def test_insert_stat_agent_queue_specific(self):
        first_call_answered = {
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:17',
                            'hangup_time': '2012-01-01 10:33:42', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4001'},
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:01',
                         'answer_time': '2012-01-01 10:33:18', 'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'queueLog': {'callid': '12345.678', 'agent': 'Agent/4001', 'queuename': 'thequeue', 'event': 'WRAPUPSTART',
                         'time': '2012-01-01 10:33:01', 'data1': 10}
        }

        second_call_answered = {
            'callOnQueue': {'callid': '12345.778', 'queue_time': '2012-01-01 10:40:00', 'answer_time': '2012-01-01 10:40:17',
                            'hangup_time': '2012-01-01 10:45:42', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4001'},
            'callData': {'uniqueid': '12345.778', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:40:00',
                         'answer_time': '2012-01-01 10:40:17', 'end_time': '2012-01-01 10:45:42', 'status': 'answer'},
            'queueLog': {'callid': '12345.778', 'agent': 'Agent/4001', 'queuename': 'thequeue', 'event': 'WRAPUPSTART',
                         'time': '2012-01-01 10:45:42', 'data1': 5}
        }

        third_call_answered = {
            'callOnQueue': {'callid': '12345.878', 'queue_time': '2012-01-01 10:35:00', 'answer_time': '2012-01-01 10:35:17',
                            'hangup_time': '2012-01-01 10:35:42', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.878', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:35:01',
                         'answer_time': '2012-01-01 10:35:18', 'end_time': '2012-01-01 10:35:43', 'status': 'answer'},
            'queueLog': {'callid': '12345.878', 'agent': 'Agent/4002', 'queuename': 'thequeue', 'event': 'WRAPUPSTART',
                         'time': '2012-01-01 10:35:01', 'data1': 10}
        }

        fourth_call_answered = {
            'callOnQueue': {'callid': '12345.978', 'queue_time': '2012-01-01 10:40:00', 'answer_time': '2012-01-01 10:40:16',
                            'hangup_time': '2012-01-01 10:45:41', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.978', 'dst_num': '5000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:40:00',
                         'answer_time': '2012-01-01 10:41:16', 'end_time': '2012-01-01 10:45:41', 'status': 'answer'},
            'queueLog': {'callid': '12345.978', 'agent': 'Agent/4002', 'queuename': 'thequeue', 'event': 'WRAPUPSTART',
                         'time': '2012-01-01 10:45:41', 'data1': 15}
        }

        fifth_call_answered = {
            'callOnQueue': {'callid': '12345.979', 'queue_time': '2012-01-01 10:44:50', 'answer_time': '2012-01-01 10:45:16',
                            'hangup_time': '2012-01-01 10:45:41', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.979', 'dst_num': '5000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:50',
                         'answer_time': '2012-01-01 10:45:16', 'end_time': '2012-01-01 10:45:41', 'status': 'answer'},
            'queueLog': {'callid': '12345.979', 'agent': 'Agent/4002', 'queuename': 'thequeue', 'event': 'WRAPUPSTART',
                         'time': '2012-01-01 10:45:41', 'data1': 15}
        }

        self.insert_stat_data(first_call_answered)
        self.insert_stat_data(second_call_answered)
        self.insert_stat_data(third_call_answered)
        self.insert_stat_data(fourth_call_answered)
        self.insert_stat_data(fifth_call_answered)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_queue_specific(self.session, start_date, end_date)
        resultats = self.session.execute('SELECT * FROM stat_agent_queue_specific')
        length = 0
        for resultat in resultats:
            length += 1
            if resultat['agent_num'] == '4001':
                self.assertEquals(resultat['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
                self.assertEquals('thequeue', resultat['queue_ref'])
                self.assertEquals(2, resultat['nb_answered_calls'])
                self.assertEquals(308, resultat['communication_time'])
                self.assertEquals(10, resultat['wrapup_time'])
                self.assertEquals('4000', resultat['dst_num'])
            elif resultat['agent_num'] == '4002' and resultat['dst_num'] == '4000':
                self.assertEquals(resultat['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
                self.assertEquals('thequeue', resultat['queue_ref'])
                self.assertEquals(1, resultat['nb_answered_calls'])
                self.assertEquals(10, resultat['wrapup_time'])
                self.assertEquals(25, resultat['communication_time'])
            elif resultat['agent_num'] == '4002' and resultat['dst_num'] == '5000':
                self.assertEquals(resultat['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
                self.assertEquals('thequeue', resultat['queue_ref'])
                self.assertEquals(1, resultat['nb_answered_calls'])
                self.assertIsNone(resultat['wrapup_time'])
                self.assertEquals(284, resultat['communication_time'])
            else:
                self.fail('Anormal')
        self.assertEquals(3, length)

        start_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 11:00:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_queue_specific(self.session, start_date, end_date)
        resultats = self.session.execute('SELECT * FROM stat_agent_queue_specific where time::varchar=\'2012-01-01 10:45:00\'')
        length = 0
        for resultat in resultats:
            length += 1
            if resultat['agent_num'] == '4001':
                self.assertEquals('thequeue', resultat['queue_ref'])
                self.assertEquals(0, resultat['nb_answered_calls'])
                self.assertEquals(42, resultat['communication_time'])
                self.assertEquals(5, resultat['wrapup_time'])
                self.assertEquals('4000', resultat['dst_num'])
            elif resultat['agent_num'] == '4002' and resultat['dst_num'] == '5000':
                self.assertEquals('thequeue', resultat['queue_ref'])
                self.assertEquals(1, resultat['nb_answered_calls'])
                self.assertEquals(30, resultat['wrapup_time'])
                self.assertEquals(66, resultat['communication_time'])
            else:
                self.fail('Anormal')
        self.assertEquals(2, length)

    def test_insert_stat_agent_queue_specific_no_call_answered(self):
        first_call = {
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:17',
                            'hangup_time': '2012-01-01 10:33:42', 'status': 'abandoned', 'queue_ref': 'thequeue', 'agent_num': '4001'},
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:01',
                         'answer_time': '2012-01-01 10:33:18', 'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'queueLog': {'callid': '12345.678', 'agent': 'Agent/4001', 'queuename': 'thequeue', 'event': 'WRAPUPSTART',
                         'time': '2012-01-01 10:33:01', 'data1': 10}
        }

        self.insert_stat_data(first_call)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_queue_specific(self.session, start_date, end_date)
        resultats = self.session.execute('SELECT * FROM stat_agent_queue_specific')
        length = 0
        for resultat in resultats:
            length += 1
            self.assertIsNotNone(resultat['agent_num'])
            self.assertIsNotNone(resultat['dst_num'])
            self.assertIsNotNone(resultat['queue_ref'])

        self.assertEquals(1, length)

    def test_insert_stat_agent_queue_specific_hold_time(self):
        first_call_answered_and_held = {
            'callOnQueue': {'callid': '12345.678', 'queue_time': '2012-01-01 10:33:00', 'answer_time': '2012-01-01 10:33:18',
                            'hangup_time': '2012-01-01 10:33:43', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.678', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:33:00',
                         'answer_time': '2012-01-01 10:33:18', 'end_time': '2012-01-01 10:33:43', 'status': 'answer'},
            'holdPeriod': {'linkedid': '12345.678', 'start': '2012-01-01 10:33:30', 'end': '2012-01-01 10:33:40'}
        }

        second_call_answered_and_held = {
            'callOnQueue': {'callid': '12345.679', 'queue_time': '2012-01-01 10:44:00', 'answer_time': '2012-01-01 10:44:18',
                            'hangup_time': '2012-01-01 10:47:40', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.679', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:44:00',
                         'answer_time': '2012-01-01 10:44:18', 'end_time': '2012-01-01 10:47:40', 'status': 'answer'},
            'holdPeriod': {'linkedid': '12345.679', 'start': '2012-01-01 10:44:50', 'end': '2012-01-01 10:45:10'}
        }

        third_call_answered_and_held = {
            'callOnQueue': {'callid': '12345.680', 'queue_time': '2012-01-01 10:27:00', 'answer_time': '2012-01-01 10:27:18',
                            'hangup_time': '2012-01-01 10:32:43', 'status': 'answered', 'queue_ref': 'thequeue', 'agent_num': '4002'},
            'callData': {'uniqueid': '12345.680', 'dst_num': '4000', 'call_direction': 'internal', 'start_time': '2012-01-01 10:27:00',
                         'answer_time': '2012-01-01 10:27:18', 'end_time': '2012-01-01 10:32:43', 'status': 'answer'},
            'holdPeriod': {'linkedid': '12345.680', 'start': '2012-01-01 10:28:30', 'end': '2012-01-01 10:28:40'}
        }

        self.insert_stat_data(first_call_answered_and_held)
        self.insert_stat_data(second_call_answered_and_held)
        self.insert_stat_data(third_call_answered_and_held)
        self.session.commit()

        start_date = datetime.strptime('2012-01-01 10:30:00', '%Y-%m-%d %H:%M:%S')
        end_date = datetime.strptime('2012-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats._fill_stat_agent_queue_specific(self.session, start_date, end_date)

        results = self.session.execute('SELECT * FROM stat_agent_queue_specific')
        length = 0
        for result in results:
            length += 1
            self.assertEquals(result['time'].strftime('%Y-%m-%d %H:%M:%S'), '2012-01-01 10:30:00')
            self.assertEquals(result['agent_num'], '4002')
            self.assertEquals(result['queue_ref'], 'thequeue')
            self.assertEquals(20, result['hold_time'])
        self.assertEquals(1, length)

    @patch('pack_reporting.additional_stats.time_manager')
    @patch('pack_reporting.additional_stats._fill_stats')
    def test_generate_stats(self, mock_fill_stats, mock_time_manager):
        date_agent = Mock(datetime)
        date_queue = Mock(datetime)
        date_agent_queue = Mock(datetime)
        end_date = Mock(datetime)
        session = Mock()
        mock_time_manager.get_agents_queue_start_date.return_value = date_agent_queue
        mock_time_manager.get_agents_start_date.return_value = date_agent
        mock_time_manager.get_queues_start_date.return_value = date_queue
        mock_time_manager.get_global_end_date.return_value = end_date

        additional_stats.generate_stats(session)

        mock_time_manager.get_queues_start_date.assert_called_once_with(session)
        mock_time_manager.get_agents_start_date.assert_called_once_with(session)
        mock_time_manager.get_agents_queue_start_date.assert_called_once_with(session)
        mock_time_manager.get_global_end_date.assert_called_once_with(session)
        mock_fill_stats.assert_called_once_with(session, date_agent, date_queue, date_agent_queue, end_date)

    @patch('pack_reporting.additional_stats.time_manager')
    @patch('pack_reporting.additional_stats._fill_stats')
    def test_generate_stats_with_provided_dates(self, mock_fill_stats, mock_time_manager):
        start_date = additional_stats.global_start_date = Mock(datetime)
        end_date = additional_stats.global_end_date = Mock(datetime)
        session = Mock()

        additional_stats.generate_stats(session)

        self.assertFalse(mock_time_manager.get_queues_start_date.called)
        self.assertFalse(mock_time_manager.get_agents_start_date.called)
        self.assertFalse(mock_time_manager.get_agents_queue_start_date.called)
        self.assertFalse(mock_time_manager.get_global_end_date.called)
        mock_fill_stats.assert_called_once_with(session, start_date, start_date, start_date, end_date)

    @patch('pack_reporting.additional_stats.time_manager')
    @patch('pack_reporting.additional_stats._fill_stat_queue_specific')
    @patch('pack_reporting.additional_stats._fill_stat_agent_specific')
    @patch('pack_reporting.additional_stats._fill_stat_agent_queue_specific')
    def test_fill_stats(self, mock_fill_agent_queue, mock_fill_agent, mock_fill_queue, mock_time_manager):
        date_agent = Mock(datetime)
        date_queue = Mock(datetime)
        date_agent_queue = Mock(datetime)
        end_date = Mock(datetime)
        d0 = Mock(datetime)
        d1 = Mock(datetime)
        d2 = Mock(datetime)
        mock_time_manager.split_interval.return_value = [(d0, d1), (d1, d2)]

        additional_stats._fill_stats(self.session, date_agent, date_queue, date_agent_queue, end_date)

        mock_fill_agent_queue.assert_has_calls([call(self.session, d0, d1), call(self.session, d1, d2)])
        mock_fill_agent.assert_has_calls([call(self.session, d0, d1), call(self.session, d1, d2)])
        mock_fill_queue.assert_has_calls([call(self.session, d0, d1), call(self.session, d1, d2)])
        mock_time_manager.split_interval.assert_has_calls([call(date_agent, end_date), call(date_queue, end_date), call(date_agent_queue, end_date)])

    def test_fix_call_on_queue_hangup_time(self):
        call_no_hangup = {
            'callOnQueue': {'callid': '12345.678',
                            'queue_time': '2013-01-01 10:28:00',
                            'answer_time': '2013-01-01 10:28:30', 
                            'status': 'answered',
                            'queue_ref': 'thequeue'},
            'xcQueueCall': {'id': 1, 'queue_ref': 'thequeue',
                            'start_time': '2013-01-01 10:28:00',
                            'end_time': '2013-01-01 10:30:30',
                            'answer_time': '2013-01-01 10:28:30',
                            'termination': 'answered',
                            'unique_id': '12345.678'}
        }

        self.insert_stat_data(call_no_hangup)
        self.session.commit()

        
        additional_stats.global_end_date = datetime.strptime('2013-01-01 10:45:00', '%Y-%m-%d %H:%M:%S')
        additional_stats.fix_call_on_queue_hangup_time(self.session)

        hangup_time = self.session.execute('SELECT hangup_time FROM call_on_queue WHERE callid=\'12345.678\'').scalar()
        self.assertEquals('2013-01-01 10:30:30', hangup_time.strftime('%Y-%m-%d %H:%M:%S'))
        additional_stats.global_end_date = None
        
    def _check_result_has_only_one_date(self, result, date):
        length = 0
        for row in result:
            self.assertEquals(date, row['time'].strftime('%Y-%m-%d %H:%M:%S'))
            length += 1
        self.assertEquals(1, length)
