from datetime import datetime, timedelta
from pack_reporting.tests.abstract_db_test import AbstractDbTest
from pack_reporting.tests.dao.call_on_queue import CallOnQueue
from pack_reporting.time_manager import TimeManager
from pack_reporting.tests.dao.queue_log import QueueLog
from mock import Mock


class TestTimeManager(AbstractDbTest):

    TABLES = ['stat_agent_specific', 'stat_queue_specific', 'stat_agent_queue_specific', 'call_on_queue', 'queue_log']

    def setUp(self):
        super(AbstractDbTest, self).setUp()
        self.manager = TimeManager(timedelta(minutes=15))
        self.session.execute('TRUNCATE %s' % ','.join(self.TABLES))

    def test_get_agent_start_date(self):
        self.session.execute('INSERT INTO stat_agent_specific("time") VALUES (:t1),(:t2)',
                             {'t1': '2012-01-01 18:15:00', 't2': '2012-01-01 18:30:00'})

        start_date = self.manager.get_agents_start_date(self.session)

        self.assertEquals('2012-01-01 18:45:00', start_date.strftime('%Y-%m-%d %H:%M:%S'))

    def test_get_agent_start_date__empty_table(self):
        self.manager.get_start_date_from_call_on_queue = Mock()
        date = Mock(datetime)
        self.manager.get_start_date_from_call_on_queue.return_value = date

        self.assertEqual(self.manager.get_agents_start_date(self.session), date)

    def test_get_queue_start_date(self):
        self.session.execute('INSERT INTO stat_queue_specific("time") VALUES (:t1),(:t2)',
                             {'t1': '2012-01-01 18:15:00', 't2': '2012-01-01 18:30:00'})

        start_date = self.manager.get_queues_start_date(self.session)

        self.assertEquals('2012-01-01 18:45:00', start_date.strftime('%Y-%m-%d %H:%M:%S'))

    def test_get_queue_start_date__empty_table(self):
        self.manager.get_start_date_from_call_on_queue = Mock()
        date = Mock(datetime)
        self.manager.get_start_date_from_call_on_queue.return_value = date

        self.assertEqual(self.manager.get_queues_start_date(self.session), date)

    def test_get_agent_queue_start_date(self):
        self.session.execute('INSERT INTO stat_agent_queue_specific("time") VALUES (:t1),(:t2)',
                             {'t1': '2012-01-01 18:15:00', 't2': '2012-01-01 18:30:00'})

        start_date = self.manager.get_agents_queue_start_date(self.session)

        self.assertEquals('2012-01-01 18:45:00', start_date.strftime('%Y-%m-%d %H:%M:%S'))

    def test_get_agent_queue_start_date__empty_table(self):
        self.manager.get_start_date_from_call_on_queue = Mock()
        date = Mock(datetime)
        self.manager.get_start_date_from_call_on_queue.return_value = date

        self.assertEqual(self.manager.get_agents_queue_start_date(self.session), date)

    def test_get_start_date_from_call_on_queue(self):
        self.session.execute(CallOnQueue.insert(values={'queue_time': '2012-01-01 08:03:00', 'callid': '123456.789'}))
        self.session.execute(CallOnQueue.insert(values={'queue_time': '2012-01-01 08:25:00', 'callid': '123456.789'}))
        self.session.commit()

        start_date = self.manager.get_start_date_from_call_on_queue(self.session)

        self.assertEquals('2012-01-01 08:00:00', start_date.strftime('%Y-%m-%d %H:%M:%S'))

    def test_round_to_interval(self):
        date = datetime.strptime('2012-01-01 08:23:00', '%Y-%m-%d %H:%M:%S')
        self.assertEquals('2012-01-01 08:15:00', TimeManager(timedelta(minutes=15)).round_to_interval(date).strftime('%Y-%m-%d %H:%M:%S'))
        self.assertEquals('2012-01-01 08:00:00', TimeManager(timedelta(hours=1)).round_to_interval(date).strftime('%Y-%m-%d %H:%M:%S'))

    def test_get_global_end_date(self):
        self.session.execute(QueueLog.insert(values={'time': '2012-01-01 18:03:00', 'callid': '123456.789'}))
        self.session.execute(QueueLog.insert(values={'time': '2012-01-01 18:25:00', 'callid': '123456.789'}))
        self.session.commit()

        start_date = self.manager.get_global_end_date(self.session)

        self.assertEquals('2012-01-01 18:15:00', start_date.strftime('%Y-%m-%d %H:%M:%S'))

    def test_split_interval(self):
        timefmt = '%Y-%m-%d %H:%M:%S'
        start = datetime.strptime('2012-01-01 08:00:00', timefmt)
        end = datetime.strptime('2012-01-01 09:15:00', timefmt)

        res = self.manager.split_interval(start, end)

        self.assertListEqual(res, [(datetime.strptime('2012-01-01 08:00:00', timefmt), datetime.strptime('2012-01-01 08:15:00', timefmt)),
                                   (datetime.strptime('2012-01-01 08:15:00', timefmt), datetime.strptime('2012-01-01 08:30:00', timefmt)),
                                   (datetime.strptime('2012-01-01 08:30:00', timefmt), datetime.strptime('2012-01-01 08:45:00', timefmt)),
                                   (datetime.strptime('2012-01-01 08:45:00', timefmt), datetime.strptime('2012-01-01 09:00:00', timefmt)),
                                   (datetime.strptime('2012-01-01 09:00:00', timefmt), datetime.strptime('2012-01-01 09:15:00', timefmt))])

    def test_date_from_string(self):
        string = '2012-05-26 08:15:30'

        res = TimeManager.date_from_string(string)

        self.assertEquals((res.year, res.month, res.day, res.hour, res.minute, res.second), (2012, 5, 26, 8, 15, 30))
