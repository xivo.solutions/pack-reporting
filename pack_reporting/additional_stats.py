# -*- coding: utf-8 -*-

# Copyright (C) 2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses>

import logging
from pack_reporting.time_manager import TimeManager
from datetime import timedelta

PERIOD = timedelta(minutes=15)
logger = logging.getLogger(__name__)
time_manager = TimeManager(PERIOD)
global_start_date = None
global_end_date = None


def clear_stats(session):
    tables_to_truncate = ['stat_agent_specific', 'stat_queue_specific']
    truncate_request = 'TRUNCATE %s' % ','.join(tables_to_truncate)
    session.execute(truncate_request)
    session.commit()


def fix_call_on_queue_hangup_time(session):
    logger.info('Fixing call_on_queue with null hangup_time according to value found in xc_queue_call')
    end_date = global_end_date if global_end_date else time_manager.get_global_end_date(session)
    fixing_cq_hangup_time_query = '''
        UPDATE call_on_queue cq
            SET hangup_time=xc_qc.end_time
            FROM xc_queue_call xc_qc
              WHERE xc_qc.unique_id=cq.callid
                AND cq.answer_time BETWEEN cast(:end_date as timestamp) - interval '1 day' AND cast(:end_date as timestamp)
                AND cq.hangup_time IS NULL
                AND xc_qc.answer_time  BETWEEN cast(:end_date as timestamp) - interval '1 day' AND cast(:end_date as timestamp)
                AND xc_qc.end_time IS NOT NULL
            RETURNING cq.id AS cq_id, xc_qc.id AS xc_qc_id, xc_qc.end_time AS end_time;
    '''
    try:
        result = session.execute(fixing_cq_hangup_time_query,
                                 {'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        if result.rowcount > 0:
            for row in result.fetchall():
                logger.info('Fixing call_on_queue id %s with hangup_time %s from xc_queue_call id %s ' % (row.cq_id, row.end_time, row.xc_qc_id))
        else:
            logger.info('No call_on_queue updated')
        session.commit()
    except Exception:
        session.rollback()
        logger.exception('Fixing call_on_queue hangup time failed')
    finally:
        logger.info('End of fixing call_on_queue hangup_time')


def generate_stats(session):
    queues_start_date = global_start_date if global_start_date else time_manager.get_queues_start_date(session)
    agents_start_date = global_start_date if global_start_date else time_manager.get_agents_start_date(session)
    agent_queue_start_date = global_start_date if global_start_date else time_manager.get_agents_queue_start_date(session)
    end_date = global_end_date if global_end_date else time_manager.get_global_end_date(session)
    _fill_stats(session, agents_start_date, queues_start_date, agent_queue_start_date, end_date)
    logger.info('End of statistic calculation')
    session.close()


def _fill_stat_queue_specific(session, start_date, end_date):
    logger.info('Generating stat queue specific between %s and %s' % (start_date, end_date))
    try:
        session.execute('DELETE FROM stat_queue_specific where time >= :start_date and time < :end_date',
                        {'start_date': start_date.strftime('%Y-%m-%d %H:%M:%S'), 'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        session.execute('SELECT 1 AS place_holder FROM insert_stat_queue_specific(:start_date, :end_date)',
                        {'start_date': start_date.strftime('%Y-%m-%d %H:%M:%S'), 'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        session.commit()
    except Exception:
        session.rollback()
        logger.exception('Queue statistic calculation failed')


def _fill_stat_agent_specific(session, start_date, end_date):
    logger.info('Generating stat agent specific between %s and %s' % (start_date, end_date))
    try:
        session.execute('DELETE FROM stat_agent_specific where time >= :start_date and time < :end_date',
                        {'start_date': start_date.strftime('%Y-%m-%d %H:%M:%S'), 'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        session.execute('SELECT 1 AS place_holder FROM insert_stat_agent_specific(:start_date, :end_date)',
                        {'start_date': start_date.strftime('%Y-%m-%d %H:%M:%S'), 'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        session.commit()
    except Exception:
        session.rollback()
        logger.exception('Agent statistic calculation failed')


def _fill_stat_agent_queue_specific(session, start_date, end_date):
    logger.info('Generating stat agent queue specific between %s and %s' % (start_date, end_date))
    try:
        session.execute('DELETE FROM stat_agent_queue_specific where time >= :start_date and time < :end_date',
                        {'start_date': start_date.strftime('%Y-%m-%d %H:%M:%S'), 'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        session.execute('SELECT 1 AS place_holder FROM insert_stat_agent_queue_specific(:start_date, :end_date)',
                        {'start_date': start_date.strftime('%Y-%m-%d %H:%M:%S'), 'end_date': end_date.strftime('%Y-%m-%d %H:%M:%S')})
        session.commit()
    except Exception:
        session.rollback()
        logger.exception('Agent queue statistic calculation failed')


def _fill_stats(session, start_date_agent, start_date_queue, start_date_agent_queue, end_date):
    logger.info('Starting specific agent stat generation between %s and %s' % (start_date_agent, end_date))
    for (start, end) in time_manager.split_interval(start_date_agent, end_date):
        _fill_stat_agent_specific(session, start, end)

    logger.info('Starting specific queue stat generation between %s and %s' % (start_date_queue, end_date))
    for (start, end) in time_manager.split_interval(start_date_queue, end_date):
        _fill_stat_queue_specific(session, start, end)

    logger.info('Starting specific agent queue generation between %s and %s' % (start_date_agent_queue, end_date))
    for (start, end) in time_manager.split_interval(start_date_agent_queue, end_date):
        _fill_stat_agent_queue_specific(session, start, end)
